# Mortgage Backend Application

This is a Java-based backend application that provides RESTful APIs for managing current interest rates and performing mortgage feasibility checks.


## Prerequisites

Java 8 or higher
Maven

## APIs
Test the application using a REST client such as Postman or cURL:

GET http://localhost:8080/api/interest-rates
Returns a list of current interest rates.

POST http://localhost:8080/api/mortgage-check
Calculates the feasibility of a mortgage based on the provided parameters and returns the monthly costs if feasible. The request body should be a JSON object with the following fields:

income : The monthly income of the borrower.
maturityPeriod : The maturity period of the mortgage in years.
loanValue : The requested loan value.
homeValue (: The value of the home being purchased.

## Global Exception Handling
The application includes global exception handling to provide consistent error responses for unexpected errors. When an unexpected error occurs, the API returns a 500 Internal Server Error response with a JSON object in the response body that includes an error message and a timestamp.

## Notes

The list of current interest rates is initialized in memory on application startup and is not persisted.

The mortgage feasibility check enforces two business rules: a mortgage should not exceed 4 times the borrower's income, and a mortgage should not exceed the value of the home being purchased.

The monthly costs of the mortgage are calculated using the formula for a fixed-rate mortgage: M = P * (r / (1 - (1 + r)^-n)), where M is the monthly payment, P is the loan value, r is the monthly interest rate, and n is the number of monthly payments over the maturity period.

The monthly interest rate is calculated based on the current interest rate for the requested maturity period.

If the mortgage is not feasible, the API returns a bad request (400) with an error message in the response body. If the mortgage is feasible, the API returns OK (200) with the monthly costs in the response body.
